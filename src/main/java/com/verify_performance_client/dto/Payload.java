package com.verify_performance_client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Payload {
    @JsonProperty("accountNumber")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer accountNumber;

    @JsonProperty("startAt")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer startAt;

    @JsonProperty("noRows")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer noRows;

//    @TODO: need to add additional fields here
}
