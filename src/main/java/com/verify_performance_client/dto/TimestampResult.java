package com.verify_performance_client.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimestampResult implements Serializable {

    private String key;
    private Long instant;

    public TimestampResult(String key) {
        this.key = key;
    }

    public String toString() {
        return "'" + key + ": " + instant + "'";
    }
}
