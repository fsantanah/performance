package com.verify_performance_client.dto.response;


import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PerformanceTestResponse implements Serializable {
    private HttpStatus status = HttpStatus.OK;
    private String message = StringUtils.EMPTY;

    private ArrayList<Object> summaryData;
    private Integer recordsRequested;
}
