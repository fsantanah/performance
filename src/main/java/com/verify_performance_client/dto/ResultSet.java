package com.verify_performance_client.dto;

import java.io.Serializable;
import java.util.Date;

public interface ResultSet extends Serializable {

    String getFirstName();
    String getLastName();
    String getMiddleName();
    Character getResults();
    Long getAccount();
    Integer getSsn();
    Date getRecvstamp();
    Date getDob();
    Boolean getRush();
    Boolean getPriority();
    String getDisplayName();
    Character getStatus();
    Long getAppKey();
    Date getDueDate();
    Date getLoginDate();
    Boolean getNumAdditionalNames();
    Boolean getAdditionalNameOnly();
    String getAppliesTo();
    Long getSeqNum();
    Character getMethod();
    Boolean getHasFaxOnFile();
    Boolean getHasNoteValue();
    Character getGender();
    Integer getCrimSrchLength();
    Integer getServiceId();
    Integer getIteration();
    String getSuffix();
    Integer getSeenBefore();
    Boolean getDisputed();
    Integer getStep();
    Byte getIsMultiState();
    Byte getError();
    Date getEta();
    String getStateBatchNum();
    Boolean getNewChaseFlag();
    Integer getInfId();
    Integer getSearcherId();
    String getState();
    Date getReceivedStamp();
    String getStatePlusCounty();
    String getScopeClock();
    Byte getAdmittedRecord();
    Character getRegion();
    String getSubKey();
}
