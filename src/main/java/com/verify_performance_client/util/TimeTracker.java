package com.verify_performance_client.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.verify_performance_client.dto.TimestampResult;
import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class TimeTracker implements Serializable {
    private HashMap<String, TimestampResult> recordedTimes;

    public TimeTracker() {
        this.recordedTimes = new HashMap<>();
    }

    public void addRecordedTime(String key, Long timestamp) {
        var timestampResult = new TimestampResult(key);
        timestampResult.setInstant(timestamp);
        recordedTimes.put(key, timestampResult);
    }

    public ArrayList<Object> getExecutionTimes() {
        var executionTimes = new ArrayList<>();
        var totalQueueActionsTime = getQueueMessageSerializationTime()
                + getQueueMessageInsertionTime()
                + getQueueMessageListenForResponseTime()
                + getQueueMessageDeserializationTime();

        executionTimes.add("Total operation run time:         " + getTotalRunTime() + " milliseconds");
        executionTimes.add("Total queue actions time:         " + totalQueueActionsTime + " milliseconds");
        executionTimes.add("Database query run time:          " + getQueryRunTime() + " milliseconds");
        executionTimes.add("Queue message serialization time: " + getQueueMessageSerializationTime() + " milliseconds");
        executionTimes.add("Queue message insertion time:     " + getQueueMessageInsertionTime() + " milliseconds");
        executionTimes.add("Listen for response time:         " + getQueueMessageListenForResponseTime() + " milliseconds");
        executionTimes.add("Deserialize queue message time:   " + getQueueMessageDeserializationTime() + " milliseconds");

        return executionTimes;
    }

    private Long getTotalRunTime() {
        var startTime = recordedTimes.get("RequestReceived").getInstant();
        var endTime = recordedTimes.get("RequestComplete").getInstant();

        return endTime - startTime;
    }

    private Long getQueryRunTime() {
        var startTime = recordedTimes.get("BeginningDatabaseQuery").getInstant();
        var endTime = recordedTimes.get("DbQueryCompleted").getInstant();

        return endTime - startTime;
    }

    private Long getQueueMessageSerializationTime() {
        var startTime = recordedTimes.get("SerializationStart").getInstant();
        var endTime = recordedTimes.get("SerializationComplete").getInstant();

        return endTime - startTime;

    }

    private Long getQueueMessageInsertionTime() {
        var startTime = recordedTimes.get("SendingToQueue").getInstant();
        var endTime = recordedTimes.get("ConvertedAndSent").getInstant();

        return endTime - startTime;

    }

    private Long getQueueMessageListenForResponseTime() {
        var startTime = recordedTimes.get("ConvertedAndSent").getInstant();
        var endTime = recordedTimes.get("ReceivedResponseFromQueue").getInstant();

        return endTime - startTime;

    }

    private Long getQueueMessageDeserializationTime() {
        var startTime = recordedTimes.get("StartSerializingQueueResponse").getInstant();
        var endTime = recordedTimes.get("FinishedSerializingQueueResponse").getInstant();

        return endTime - startTime;
    }
}
