package com.verify_performance_client.util;

import java.util.UUID;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ScheduledMessage;

@Component
public class Queue {
    private final static String QUEUE_CORRELATION_SELECTOR = "JMSCorrelationID='%s'";
    private final JmsTemplate jmsTemplate;
    private final TimeTracker timeTracker;

    public Queue(JmsTemplate jmsTemplate, TimeTracker timeTracker) {
        this.jmsTemplate = jmsTemplate;
        this.timeTracker = timeTracker;
    }

    /**
     * @param queueName
     * @param queueReplyName
     * @param queueData
     * @param deliveryDelay
     * @param receiveTimeOut
     * @return
     */
    public JsonNode sendAndWaitForReply(String queueName, String queueReplyName, Object queueData, Long deliveryDelay, Long receiveTimeOut) throws JsonProcessingException {
        final String correlationid = UUID.randomUUID().toString();
        final String selector = String.format(QUEUE_CORRELATION_SELECTOR, correlationid);

        timeTracker.addRecordedTime("SerializationStart", System.currentTimeMillis());
        ObjectMapper objectMapper = new ObjectMapper();
        String queueDataString = objectMapper.writeValueAsString(queueData);
        timeTracker.addRecordedTime("SerializationComplete", System.currentTimeMillis());

        jmsTemplate.setReceiveTimeout(receiveTimeOut);

        timeTracker.addRecordedTime("SendingToQueue", System.currentTimeMillis());
        jmsTemplate.convertAndSend(queueName, queueDataString, createMessagePostProcessor(correlationid, deliveryDelay, queueReplyName));
        timeTracker.addRecordedTime("ConvertedAndSent", System.currentTimeMillis());

        var result = jmsTemplate.receiveSelectedAndConvert(queueReplyName, selector);
        timeTracker.addRecordedTime("ReceivedResponseFromQueue", System.currentTimeMillis());

        timeTracker.addRecordedTime("StartSerializingQueueResponse", System.currentTimeMillis());
        JsonNode node = objectMapper.readTree((String) result);
        timeTracker.addRecordedTime("FinishedSerializingQueueResponse", System.currentTimeMillis());
        return  node;
    }

    /**
     * @param correlationId
     * @param deliveryDelay
     * @param queueReplyName
     * @return
     */
    private MessagePostProcessor createMessagePostProcessor(
            String correlationId,
            Long deliveryDelay,
            String queueReplyName
    ) {
        return message -> {
            if (queueReplyName != null)
                message.setStringProperty("JMSReplyTo", queueReplyName);

            if (correlationId != null)
                message.setJMSCorrelationID(correlationId);

            if (deliveryDelay != null)
                message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, deliveryDelay);

            message.setLongProperty("JMSTimeToLive", 3600);

            return message;
        };
    }
}
