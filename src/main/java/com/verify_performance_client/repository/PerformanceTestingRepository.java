package com.verify_performance_client.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.verify_performance_client.dto.ResultSet;
import com.verify_performance_client.entity.CrimList;

@Repository
public interface PerformanceTestingRepository extends PagingAndSortingRepository<CrimList, Long> {

    @Query(value = "SELECT TOP (:numberOfRows) \n" +
            "       UPPER(crimlist.State + ' ' + crimlist.County),\n" +
            "       applist.lastname,\n" +
            "       applist.firstname,\n" +
            "       applist.middlename,\n" +
            "       crimlist.results,\n" +
            "       applist.account,\n" +
            "       applist.ssn,\n" +
            "       applist.recvstamp,\n" +
            "       applist.dob,\n" +
            "       applist.rush,\n" +
            "       applist.priority,\n" +
            "       searchers.displayname,\n" +
            "       crimlist.status,\n" +
            "       crimlist.appkey,\n" +
            "       crimlist.state + substring(crimlist.county, 1, 15)                                      as SubKey,\n" +
            "       'A'                                                                                     as Service,\n" +
            "       applist.duedate,\n" +
            "       applist.logindate,\n" +
            "       crimlist.HasAdditionalNames                                                             as NumAdditionalNames,\n" +
            "       crimlist.AdditionalNameOnly,\n" +
            "       SpecialInstruct.AppliesTo,\n" +
            "       ''                                                                                      as AppName,\n" +
            "       crimlist.seqnum,\n" +
            "       searchers.method,\n" +
            "       CASE\n" +
            "           WHEN Jurisdictions.Region IS NULL THEN\n" +
            "               CASE\n" +
            "                  WHEN CrimList.State IN ('CP', 'IS') THEN International.Region\n" +
            "                ELSE NULL END\n" +
            "           ELSE Jurisdictions.Region END                                                       AS region,\n" +
            "       applist.hasfax                                                                          as FaxOnFile,\n" +
            "       crimlist.hasnote                                                                        as HasNoteValue,\n" +
            "       applist.sex                                                                             as Gender,\n" +
            "       crimlist.CrimSrchLength,\n" +
            "       crimlist.serviceid,\n" +
            "       crimlist.Iteration,\n" +
            "       applist.suffix,\n" +
            "       case when applist.admittedcriminalrecord is null then 0 else 1 end                      as AdmittedRecord,\n" +
            "       crimlist.seenbefore,\n" +
            "       services.disputed,\n" +
            "       services.step,\n" +
            "       crimlist.ismultistate,\n" +
            "       services.error,\n" +
            "       PostponeReason.ETA,\n" +
            "       crimlist.StateBatchNum,\n" +
            "       crimlist.NewChaseFlag,\n" +
            "       applist.InfID,\n" +
            "       Convert(VarChar(10), DATEADD(Year, -1 * (3 + crimlist.CrimSrchLength), GetDate()), 101) as ScopeClock,\n" +
            "       crimlist.searcherid,\n" +
            "       crimlist.state,\n" +
            "       services.ReceivedStamp\n" +
            "FROM ASAPDATA.dbo.applist\n" +
            "         JOIN ASAPDATA.dbo.Services ON AppList.AppKey = Services.AppKey\n" +
            "         JOIN ASAPDATA.dbo.CrimList ON Services.ServiceID = CrimList.ServiceID\n" +
            "         left outer join ASAPDATA.dbo.SpecialInstruct ON crimlist.appkey = SpecialInstruct.appkey\n" +
            "         left outer join ASAPDATA.dbo.searchers on crimlist.searcherid = searchers.searcherid\n" +
            "         left outer join ASAPDATA.dbo.Jurisdictions\n" +
            "                         on CrimList.State = Jurisdictions.State AND CrimList.County = Jurisdictions.County\n" +
            "         left outer join ASAPDATA.dbo.PostponeReason on CrimList.ServiceID = PostponeReason.ServiceID\n" +
            "         outer apply(SELECT Region FROM ASAPDATA.dbo.EmployeeRegion WHERE International = 1) International\n" +
            "WHERE Services.Service = 'A'\n" +
            "  AND AppList.AppType = 1\n" +
            "  and Services.Status = 'C' \n", nativeQuery = true)
    ArrayList<ResultSet> getResultSet(@Param("numberOfRows") Integer numberOfRows);
}