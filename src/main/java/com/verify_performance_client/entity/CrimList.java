package com.verify_performance_client.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

@Entity
@Getter
@Setter
@Table(name = "CrimList", catalog = "ASAPDATA", schema = "dbo")
@Immutable
public class CrimList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "seqnum", nullable = false)
    private Integer caseListId;

    @Column(name = "appkey", nullable = false)
    private Long applicantKey;

    @Column(name = "State", nullable = false, length = 2)
    private String state;

    @Column(name = "County", nullable = false, length = 45)
    private String county;

    @Column(name = "SearcherID", scale = 6)
    private Integer searcherId;

    @Column(name = "Results")
    private String results;

    @Column(name = "AdditionalNameOnly", nullable = false)
    private Boolean additionalNameOnly = false;

    @Column(name = "Region")
    private String region;

    @Column(name = "WebPrintStamp")
    private LocalDateTime webPrintStamp;

    @Column(name = "WebPrintOrder")
    private Integer webPrintOrder;

    @Column(name = "ServiceID")
    private Long serviceId;

    @Column(name = "HasAdditionalNames", nullable = false)
    private Boolean hasAdditionalNames = false;

    @Column(name = "FromNSSS", nullable = false)
    private Boolean fromNSSS = false;

    @Column(name = "CrimSrchLength", nullable = false)
    private Integer crimSrchLength = 7;

    @Column(name = "HasNote", nullable = false)
    private Boolean hasNote = false;

    @Column(name = "Status", nullable = false)
    private String status = "P";

    @Column(name = "NsssJurNum")
    private Integer nsssJurNum;

    @Column(name = "TestStamp")
    private LocalDateTime testStamp;

    @Column(name = "HaveGender", nullable = false)
    private Boolean haveGender = false;

    @Column(name = "HaveRelease", nullable = false)
    private Boolean haveRelease = false;

    @Column(name = "HaveAddress", nullable = false)
    private Boolean haveAddress = false;

    @Column(name = "isMultiState", nullable = false)
    private Boolean IsMultiState = false;

    @Column(name = "AddressOnRelease", nullable = false)
    private Boolean addressOnRelease = false;

    @Column(name = "City", length = 28)
    private String city;

    @Column(name = "SentToVendor", nullable = false)
    private Boolean sentToVendor = false;

    @Column(name = "Iteration", nullable = false)
    private Integer iteration = 1;

    @Column(name = "SeenBefore", nullable = false)
    private Integer seenBefore = 2;

    @Column(name = "webPrintStampBy")
    private Integer webPrintStampBy;

    @Column(name = "StateBatchNum", length = 15)
    private String stateBatchNum;

    @Column(name = "ChasedParentServiceID")
    private Integer chasedParentServiceID;

    @Column(name = "NewChaseFlag", nullable = false)
    private Boolean newChaseFlag = false;
}
