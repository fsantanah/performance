package com.verify_performance_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerifyPerformanceClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(VerifyPerformanceClientApplication.class, args);
    }

}
