package com.verify_performance_client.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.verify_performance_client.dto.ResultSet;
import com.verify_performance_client.repository.PerformanceTestingRepository;
import com.verify_performance_client.util.Queue;

@Service
public class PerformanceTestingService {

    private final PerformanceTestingRepository repository;

    private final Queue queue;

    public PerformanceTestingService(PerformanceTestingRepository repository, Queue queue) {
        this.repository = repository;
        this.queue = queue;
    }

    public List<ResultSet> runVerifyQuery(Integer numberOfRows) {
        return repository.getResultSet(numberOfRows);
    }

    public JsonNode sendResultsToQueue(Object results) throws JsonProcessingException {
        return  queue.sendAndWaitForReply("speed_test", "speed_test", results, 0L, 0L);
    }

}
