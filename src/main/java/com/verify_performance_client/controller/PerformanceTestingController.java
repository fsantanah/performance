package com.verify_performance_client.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.verify_performance_client.dto.response.PerformanceTestResponse;
import com.verify_performance_client.service.PerformanceTestingService;
import com.verify_performance_client.util.TimeTracker;

@RestController
@RequestMapping("/v1")
public class PerformanceTestingController {

    private final PerformanceTestingService service;

    private final TimeTracker timeTracker;

    public PerformanceTestingController(PerformanceTestingService service, TimeTracker timeTracker) {
        this.service = service;
        this.timeTracker = timeTracker;
    }

    @GetMapping(value = "/result_set")
    public ResponseEntity<Object> testGetResultSet(@RequestParam Integer numberOfRows) throws JsonProcessingException {
        System.out.println("the number of rows requested is: " + numberOfRows);

        timeTracker.addRecordedTime("RequestReceived", System.currentTimeMillis());

        timeTracker.addRecordedTime("BeginningDatabaseQuery", System.currentTimeMillis());
        var results = service.runVerifyQuery(numberOfRows);

        timeTracker.addRecordedTime("DbQueryCompleted", System.currentTimeMillis());
        var sendToQueueCount = results.size();
        JsonNode queueReply = service.sendResultsToQueue(results);

        var receivedFromQueueCount = 0;
        try {
           receivedFromQueueCount = queueReply.size();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        timeTracker.addRecordedTime("RequestComplete", System.currentTimeMillis());
        var performanceTestResponse = new PerformanceTestResponse();
        performanceTestResponse.setSummaryData(timeTracker.getExecutionTimes());
        performanceTestResponse.setMessage("Number sent to queue: " + sendToQueueCount + " number received: " + receivedFromQueueCount);
        performanceTestResponse.setRecordsRequested(numberOfRows);
        return ResponseEntity.ok(performanceTestResponse);
    }
}
